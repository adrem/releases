<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	
	<modelVersion>4.0.0</modelVersion>
	<groupId>be.uantwerpen</groupId>
	<artifactId>rmis</artifactId>
	<version>3.0.0</version>
	
	<properties>
		<java.version>1.8</java.version>
		<classpath.mainClass>be.uantwerpen.algorithms.itemsets.RMISDriver</classpath.mainClass>
	</properties>
	
	<dependencies>
	
		<!-- https://mvnrepository.com/artifact/com.google.guava/guava -->
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>25.0-jre</version>
		</dependency>
		
		<dependency>
			<groupId>org.realkd</groupId>
			<artifactId>realKD</artifactId>
			<version>0.7.1</version>
		</dependency>
    
	</dependencies>
	
	<repositories>
	
 		<repository>
			  <id>localrepo</id>
			  <url>file:${project.basedir}/lib</url>
		</repository>
		
		<repository>
			<id>realKD-releases</id>
			<url>https://bitbucket.org/realKD/releases/raw/master/releases</url>
			<releases>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy>
				<checksumPolicy>fail</checksumPolicy>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy>
				<checksumPolicy>warn</checksumPolicy>
			</snapshots>
		</repository>

	</repositories>
	
	<build>
	
		<sourceDirectory>src/main/java</sourceDirectory>
		
		<resources>
			<resource>
				<directory>src</directory>
				<excludes>
					<exclude>**/*.java</exclude>
				</excludes>
			</resource>
		</resources>
		
		<plugins>
			
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.6.1</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin>
			
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.0.2</version>
				
				<configuration>
					<archive>
						<manifest>
							<mainClass>${classpath.mainClass}</mainClass>
						</manifest>
					</archive>
				</configuration>
			</plugin>
			
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				
				<executions>
				
					<execution>
						<id>jar-with-dependencies</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
						<configuration>
							<descriptorRefs>
								<descriptorRef>jar-with-dependencies</descriptorRef>
							</descriptorRefs>
							<archive>
								<index>true</index>
								<manifest>
									<addClasspath>true</addClasspath>
									<mainClass>${classpath.mainClass}</mainClass>
								</manifest>
							</archive>
						</configuration>
					</execution>
					
					<execution>
						<id>build-zip-file</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
						<configuration>
							<descriptors>
								<descriptor>src/assembly/package.xml</descriptor>
							</descriptors>
						</configuration>
					</execution>
					
				</executions>
				
			</plugin>
			
		</plugins>
		
	</build>

	<distributionManagement>
	
		<repository>
			<id>local.release.repo</id>
			<name>Local release repository</name>
			<url>file:../releases</url>
		</repository>
	
	</distributionManagement>
	
</project>
